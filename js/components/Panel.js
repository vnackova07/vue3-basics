export default {
    template: ` 
    <div class="{
        'p-2 border rounded-lg': true,
        'bg-white border-gray-300' : theme == 'light'
        'bg-gray-700 border-gray-600' : theme == 'dark'
    }">
    <h2 v-if="$slots.heading" class="font-bold mb-2">
        <slot name="heading"/>
    </h2>

    <slot/>

    <footer v-if="$slots.footer" class="border-t border-gray-600 mt-4 pt-4 text-sm">
        <slot name="footer"></slot>
    </footer>
    </div>
    
    `,
    props: {
        theme: { type: String, default: 'dark'}
    }
}